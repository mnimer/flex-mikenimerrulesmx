package com.mikenimer.rules
{
	import flash.events.Event;

	public class RuleEvent extends Event
	{
		public static const INVALIDATERULE:String = "invalidateRule";
		
		public function RuleEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}