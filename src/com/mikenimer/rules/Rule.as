package com.mikenimer.rules
{

import flash.events.EventDispatcher;

import mx.core.UIComponent;
import mx.core.mx_internal;
import mx.states.IOverride;

import com.mikenimer.rules.RuleEvent;

use namespace mx_internal;

//--------------------------------------
//  Other metadata
//--------------------------------------
[DefaultProperty("overrides")]

/**
 *  The <code>overrides</code> property specifies a set of child classes
 *  to add or remove from the base view state, and properties, styles, and event
 *  handlers to set when the view state is in effect.
 *
 *  @mxml
 *  <p>The <code>&lt;mx:Rule&gt;</code> tag has the following attributes:</p>
 *
 *  <pre>
 *  &lt;Rule
 *  <b>Properties</b>
 *  expression="{...binding...}"
 *  /&gt;
 *  </pre>
 */
 	
	public class Rule extends EventDispatcher
	{
		private var childrentInitialized:Boolean = false;	
		public var parent:UIComponent = null;
				
		
	    //----------------------------------
		//  expression
	    //----------------------------------
	
		[Inspectable(category="General")]
	
		/**
		 *  The name of the view state upon which this view state is based, or
		 *  <code>null</code> if this view state is not based on a named view state.
		 *  If this value is <code>null</code>, the view state is based on a root
		 *  state that consists of the properties, styles, event handlers, and
		 *  children that you define for a component without using a State class.
		 *
		 *  @default null
		 */
		private var _expression:Boolean;
		
		public function get expression():Boolean
		{
			return _expression;
		}
		
		public function set expression(b:Boolean):void
		{
			if( _expression != b )
			{
				_expression = b;		
				dispatchEvent( new RuleEvent(RuleEvent.INVALIDATERULE, false, false) );
			}
		}
	
	    //----------------------------------
		//  overrides
	    //----------------------------------
	
		[ArrayElementType("mx.states.IOverride")]
		[Inspectable(category="General")]
	
		/**
		 *  The overrides for this view state, as an Array of objects that implement
		 *  the IOverride interface. These overrides are applied in order when the
		 *  state is entered, and removed in reverse order when the state is exited.
		 *
		 *  <p>The following Flex classes implement the IOverride interface and let you
		 *  define the view state characteristics:</p>
		 *  <ul>
		 * 		<li>AddChild</li>
		 * 		<li>RemoveChild</li>
		 * 		<li>SetEventHandler</li>
		 * 		<li>SetProperty</li>
		 * 		<li>SetStyle</li>
		 *  </ul>
		 *
		 *  <p>The <code>overrides</code> property is the default property of the
		 *  State class. You can omit the <code>&lt;mx:overrides&gt;</code> tag
		 *  and its child <code>&lt;mx:Array&gt;</code>tag if you use MXML tag
		 *  syntax to define the overrides.</p>
		 */
		public var overrides:Array /* of IOverride */ = [];
	
	    //--------------------------------------------------------------------------
	    //
	    //  Methods
	    //
	    //--------------------------------------------------------------------------
	
	    /**
	     *  @private
	     *  Initialize this state and all of its overrides.
	     */
	    //todo: use namespace
	    public function initialize():void
	    {
	    	if (!childrentInitialized)
	    	{
	    		childrentInitialized = true;
	    		for (var i:int = 0; i < overrides.length; i++)
	    		{
	    			IOverride(overrides[i]).initialize();
	    		}
	    	}
	    }
	    	
	
		public function apply():void
	    {
	    	for (var i:int = 0; i < overrides.length; i++)
			{
				//IOverride(overrides[i]).apply(parent)
				IOverride(overrides[i]).apply(parent);
			}
	    }
	
		public function remove():void
	    {
	    	for (var i:int = 0; i < overrides.length; i++)
			{
				//IOverride(overrides[i]).apply(parent)
				IOverride(overrides[i]).remove(parent);
			}
	    }
	}

}
