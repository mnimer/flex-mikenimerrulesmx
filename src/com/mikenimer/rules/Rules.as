package com.mikenimer.rules
{

import flash.events.Event;

import mx.core.UIComponent;
import mx.core.mx_internal;
import mx.events.FlexEvent;

use namespace mx_internal;

//--------------------------------------
//  Other metadata
//--------------------------------------
[DefaultProperty("rules")]

/**
 *  The <code>overrides</code> property specifies a set of child classes
 *  to add or remove from the base view state, and properties, styles, and event
 *  handlers to set when the view state is in effect.
 *
 *  @mxml
 *  <p>The <code>&lt;mx:Rule&gt;</code> tag has the following attributes:</p>
 *
 *  <pre>
 *  &lt;Rule
 *  <b>Properties</b>
 *  expression="{...binding...}"
 *  /&gt;
 *  </pre>
 */
 	
	public class Rules extends UIComponent
	{
		private var mxmlInitialized:Boolean = false;
		private var childrentInitialized:Boolean = false;	
			
		public var rules:Array = [];
		private var ruleApplyQueue:Array = []; 
		private var ruleRemoveQueue:Array = []; 
				
		public function Rules()
		{
		}
		
		
	    override protected function childrenCreated():void
	    {
	    	super.childrenCreated();
	        initializeChildren();
	    }
		
		
		private function initializeChildren():void
	    {
	    	if (!childrentInitialized)
	    	{
	    		childrentInitialized = true;
	    		for (var i:int = 0; i < rules.length; i++)
	    		{
	    			rules[i].addEventListener(RuleEvent.INVALIDATERULE, invalidRuleHandler, false, 0, true);
	    		}
	    	}
	    }	
	    
	    
	    private function invalidRuleHandler(event:Event):void
	    {
	    	event.stopPropagation();
	    	if( event.currentTarget is Rule )
	    	{
	    		if( Rule(event.currentTarget).expression )
	    		{
	    			ruleApplyQueue.push(event.currentTarget);
	    			invalidateDisplayList();
	    		}
	    		else
	    		{
	    			ruleRemoveQueue.push(event.currentTarget);
	    			invalidateDisplayList();
	    		}
	    	}
	    }
	    
	    
	    override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
	    {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			// First undo all of the effected rules
			while( ruleRemoveQueue.length > 0 )
			{
				var r1:Rule = ruleRemoveQueue.pop();
					r1.remove();
			}
			// then apply the new rules settings
			while( ruleApplyQueue.length > 0 )
			{
				var r2:Rule = ruleApplyQueue.pop();
					r2.apply()
			}
	    }
	}

}
