package com.mikenimer.rules
{
	import flash.display.DisplayObject;
	
	import mx.core.Application;
	import mx.core.IDeferredInstance;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	import mx.states.IOverride;

	[DefaultProperty("targetFactory")]

	public class AddPopUp implements IOverride
	{
		private var instanceCreated:Boolean = false;
	    public var parent:DisplayObject;
	    public var modal:Boolean = true;
	    
        //------------------------------------
	    //  targetFactory
	    //------------------------------------
	    
	    /**
	     *  @private
	     *  Storage for the targetFactory property.
	     */
	    private var _targetFactory:IDeferredInstance;

	    /**
	     *
	     * The factory that creates the child. You can specify either of the following items:
	     *  <ul>
	     *      <li>A factory class that implements the IDeferredInstance
	     *          interface and creates the child instance or instances.
	     *      </li>
	     *      <li>A Flex component, (that is, any class that is a subclass
	     *          of the UIComponent class), such as the Button contol.
	     *          If you use a Flex component, the Flex compiler automatically
	     *          wraps the component in a factory class.
	     *      </li>
	     *  </ul>
	     *
	     *  <p>If you set this property, the child is instantiated at the time
	     *  determined by the <code>creationPolicy</code> property.</p>
	     *  
	     *  <p>Do not set this property if you set the <code>target</code>
	     *  property.
	     *  This propety is the <code>AddChild</code> class default property.
	     *  Setting this property with a <code>creationPolicy</code> of "all"
	     *  is equivalent to setting a <code>target</code> property.</p>
	     */
	    public function get targetFactory():IDeferredInstance
	    {
	        return _targetFactory;
	    }
	
	    /**
	     *  @private
	     */
	    public function set targetFactory(value:IDeferredInstance):void
	    {
	        _targetFactory = value;
            createInstance();
	    }
	

	    //------------------------------------
	    //  target
	    //------------------------------------
	
	    /**
	     *  @private
	     *  Storage for the target property
	     */
	    private var _target:IFlexDisplayObject;
	
	    [Inspectable(category="General")]
	
	    /**
	     *
	     *  The child to be added.
	     *  If you set this property, the child instance is created at app startup.
	     *  Setting this property is equivalent to setting a <code>targetFactory</code>
	     *  property with a <code>creationPolicy</code> of <code>"all"</code>.
	     *
	     *  <p>Do not set this property if you set the <code>targetFactory</code>
	     *  property.</p>
	     */
	    public function get target():IFlexDisplayObject
	    {
	        if (!_target)
	            createInstance();
	
	        return _target;
	    }
	
	    /**
	     *  @private
	     */
	    public function set target(value:IFlexDisplayObject):void
	    {
	        _target = value;
	    }
		

	    /**
	     *  Creates the child instance from the factory.
	     *  You must use this method only if you specify a <code>targetFactory</code>
	     *  property and a <code>creationPolicy</code> value of <code>"none"</code>.
	     *  Flex automatically calls this method if the <code>creationPolicy</code>
	     *  property value is <code>"auto"</code> or <code>"all"</code>.
	     *  If you call this method multiple times, the child instance is
	     *  created only on the first call.
	     */
	    public function createInstance():void
	    {
	        if (!instanceCreated && !_target && targetFactory)
	        {
	            instanceCreated = true;
	            var instance:Object = targetFactory.getInstance();
	            if (instance is IFlexDisplayObject)
	                _target = IFlexDisplayObject(instance);
	        }
	    }
	

	    /**
	     *  @inheritDoc
	     */
	    public function initialize():void
	    {
	     	createInstance();
	    }

		
		public function apply(parent:UIComponent):void
		{
			// Early exit if child is null
	        if (!target)
	            return;
	            
	        PopUpManager.addPopUp(target, this.parent, modal)
	        PopUpManager.centerPopUp(target);
		}

		
		public function remove(parent:UIComponent):void
		{
			PopUpManager.removePopUp(target);
		}
		
		
		public function AddPopUp()
		{
			super();
		}		
	}
}